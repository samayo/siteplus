<?php

namespace RentBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use RentBundle\Entity\Post;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Response;


class PagesController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction()
    {


        $repository = $this->getDoctrine()->getRepository('RentBundle:Post');

        $posts = $repository->findAll();


        return $this->render('RentBundle:Pages:index.html.twig', array(
            'posts' => $posts
        ));
    }

    /**
     * @Route("/show/{id}/{slug}", name="show_announce", requirements={"id" : "\d+"})
     */
    public function showAction($id)
    {
        $postRepository = $this->getDoctrine()->getRepository('RentBundle:Post');
        $ad = $postRepository->find($id);

        return $this->render('RentBundle:Pages:show.html.twig', array(
            'ad' => $ad
        ));
    }


    /**
     * @Route("/post", name="post_announce")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function postAction(Request $request)
    {
        $post = new Post();
        $form = $this->createFormBuilder($post)
                ->add('title', TextType::class)
                ->add('address', TextType::class)
                ->add('price', TextType::class)
                ->add('floor', TextType::class)
                ->add('submit', SubmitType::class)
                ->getForm();


        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $post->setSlug('room-in-sion');

            $post = $form->getData();

            $em = $this->getDoctrine()->getManager();
            $em->persist($post);
            $em->flush();

            return $this->redirectToRoute('task_success');
        }

        return $this->render('RentBundle:Pages:post.html.twig', [
            'form' => $form->createView(),
        ]);


//        return $this->render('RentBundle:Pages:post.html.twig', array(
//            // ...
//        ));
    }

    /**
     * @Route("/delete/{id}", name="delete_announce")
     */
    public function deleteAction()
    {
        return $this->render('RentBundle:Pages:delete.html.twig', array(
            // ...
        ));
    }

    /**
     * @Route("/edit/{id}", name="edit_announce")
     */
    public function editAction($id, Request $request)
    {


        $em = $this->getDoctrine()->getManager();

        $post = $em->getRepository('RentBundle:Post')->find($id);

        if(!$id){
            throw new $this->createNotFoundException(
                'No post found for id '. $id
            );
        }


        $form = $this->createFormBuilder($post)
            ->add('title', TextType::class)
            ->add('address', TextType::class)
            ->add('price', TextType::class)
            ->add('floor', TextType::class)
            ->add('submit', SubmitType::class)
            ->getForm();


        $form->handleRequest($request);


        if($form->isValid()){
            $em->flush();
            $response = new Response('Post updated with success');
            $response->setStatusCode(200);
            $response->headers->set('Refresh', '5 url=/');

            $response->send();

        }

        return $this->render('RentBundle:Pages:post.html.twig', [
            'form' => $form->createView(),
        ]);

//        return $this->render('RentBundle:Pages:edit.html.twig', array(
//            // ...
//        ));
    }



    /**
     * @Route("/success", name="task_success")
     */
    public function postSuccessAction(){
        $response = new Response("Thank you for your submission");

        $response->setStatusCode(200);
        $response->headers->set('Refresh', '5; url=/');

        $response->send();
    }

    /**
     * @Route("/admin")
     * @return Response
     */
    public function adminAction()
    {
        return new Response("YO, Admin");
    }
}
