<?php
/**
 * Created by PhpStorm.
 * User: sdaniel
 * Date: 10.02.2017
 * Time: 11:26
 */

namespace RentBundle\Menu;


use Knp\Menu\MenuFactory;

class Builder
{
    public function mainMenu(MenuFactory $factory, array $options)
    {
        $menu = $factory->createItem('root');
        $menu->setChildrenAttribute('class', 'nav-item is-tab');
        $menu->addChild('Site+', ['route' => 'home_page']);
        return $menu;
    }

    public function addMenu(MenuFactory $factory, array $options)
    {
        $menu = $factory->createItem('root');
        $menu->setChildrenAttribute('class', 'nav-item is-tab');
        $menu->addChild('Add', ['route' => 'post_announce']);
        return $menu;
    }

    public function loginMenu(MenuFactory $factory, array $options)
    {
        $menu = $factory->createItem('root');
        $menu->setChildrenAttribute('class', 'nav-item is-tab');
        $menu->addChild('Login', ['route' => 'fos_user_security_login']);
        return $menu;
    }

    public function registerMenu(MenuFactory $factory, array $options)
    {
        $menu = $factory->createItem('root');
        $menu->setChildrenAttribute('class', 'nav-item is-tab');
        $menu->addChild('Register', ['route' => 'fos_user_registration_register']);
        return $menu;
    }

    public function logoutMenu(MenuFactory $factory, array $options)
    {
        $menu = $factory->createItem('root');
        $menu->setChildrenAttribute('class', 'nav-item is-tab');
        $menu->addChild('Logout', ['route' => 'fos_user_security_logout']);
        return $menu;
    }
}