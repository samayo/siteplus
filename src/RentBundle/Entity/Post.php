<?php

namespace RentBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Post
 *
 * @ORM\Table(name="post")
 * @ORM\Entity(repositoryClass="RentBundle\Repository\PostRepository")
 */
class Post
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=255)
     */
    private $slug;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=255)
     */
    private $address;

    /**
     * @var int
     *
     * @ORM\Column(name="houseNumber", type="integer")
     */
    private $houseNumber;

    /**
     * @var int
     *
     * @ORM\Column(name="zipCode", type="integer")
     */
    private $zipCode;

    /**
     * @var string
     *
     * @ORM\Column(name="propertyType", type="string", length=255)
     */
    private $propertyType;

    /**
     * @var string
     *
     * @ORM\Column(name="area", type="string", length=255)
     */
    private $area;

    /**
     * @var int
     *
     * @ORM\Column(name="rooms", type="integer")
     */
    private $rooms;

    /**
     * @var int
     *
     * @ORM\Column(name="floor", type="integer")
     */
    private $floor;

    /**
     * @var bool
     *
     * @ORM\Column(name="hasParking", type="boolean")
     */
    private $hasParking;

    /**
     * @var bool
     *
     * @ORM\Column(name="hasLift", type="boolean")
     */
    private $hasLift;

    /**
     * @var bool
     *
     * @ORM\Column(name="hasBunker", type="boolean")
     */
    private $hasBunker;

    /**
     * @var bool
     *
     * @ORM\Column(name="hasBalcon", type="boolean")
     */
    private $hasBalcon;

    /**
     * @var bool
     *
     * @ORM\Column(name="isFurnished", type="boolean")
     */
    private $isFurnished;

    /**
     * @var int
     *
     * @ORM\Column(name="price", type="integer")
     */
    private $price;

    /**
     * @var int
     *
     * @ORM\Column(name="charges", type="integer")
     */
    private $charges;

    /**
     * @var string
     *
     * @ORM\Column(name="parkingFee", type="string", length=255)
     */
    private $parkingFee;

    /**
     * @var string
     *
     * @ORM\Column(name="hasInternet", type="string", length=255)
     */
    private $hasInternet;

    /**
     * @var bool
     *
     * @ORM\Column(name="hasTelephone", type="boolean")
     */
    private $hasTelephone;

    /**
     * @var int
     *
     * @ORM\Column(name="tel", type="integer")
     */
    private $tel;

    /**
     * @var string
     *
     * @ORM\Column(name="info", type="text")
     */
    private $info;

    /**
     * @var string
     *
     * @ORM\Column(name="currentTenant", type="string", length=255)
     */
    private $currentTenant;

    /**
     * @var string
     *
     * @ORM\Column(name="contactPerson", type="string", length=255)
     */
    private $contactPerson;

    /**
     * @var bool
     *
     * @ORM\Column(name="isPostActive", type="boolean")
     */
    private $isPostActive;

    /**
     * @var bool
     *
     * @ORM\Column(name="visitationDate", type="boolean")
     */
    private $visitationDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="availableFrom", type="datetime")
     */
    private $availableFrom;

    /**
     * @var bool
     *
     * @ORM\Column(name="availableUpto", type="boolean")
     */
    private $availableUpto;

    /**
     * @var int
     *
     * @ORM\Column(name="randomId", type="integer")
     */
    private $randomId;

    /**
     * @var string
     *
     * @ORM\Column(name="postedBy", type="string", length=255)
     */
    private $postedBy;

    /**
     * @var string
     *
     * @ORM\Column(name="crawlerSource", type="string", length=255)
     */
    private $crawlerSource;

    /**
     * @var string
     *
     * @ORM\Column(name="externalLink", type="string", length=255)
     */
    private $externalLink;

    /**
     * @var int
     *
     * @ORM\Column(name="userId", type="integer")
     */
    private $userId;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return Post
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Post
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return Post
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set houseNumber
     *
     * @param integer $houseNumber
     *
     * @return Post
     */
    public function setHouseNumber($houseNumber)
    {
        $this->houseNumber = $houseNumber;

        return $this;
    }

    /**
     * Get houseNumber
     *
     * @return int
     */
    public function getHouseNumber()
    {
        return $this->houseNumber;
    }

    /**
     * Set zipCode
     *
     * @param integer $zipCode
     *
     * @return Post
     */
    public function setZipCode($zipCode)
    {
        $this->zipCode = $zipCode;

        return $this;
    }

    /**
     * Get zipCode
     *
     * @return int
     */
    public function getZipCode()
    {
        return $this->zipCode;
    }

    /**
     * Set propertyType
     *
     * @param string $propertyType
     *
     * @return Post
     */
    public function setPropertyType($propertyType)
    {
        $this->propertyType = $propertyType;

        return $this;
    }

    /**
     * Get propertyType
     *
     * @return string
     */
    public function getPropertyType()
    {
        return $this->propertyType;
    }

    /**
     * Set area
     *
     * @param string $area
     *
     * @return Post
     */
    public function setArea($area)
    {
        $this->area = $area;

        return $this;
    }

    /**
     * Get area
     *
     * @return string
     */
    public function getArea()
    {
        return $this->area;
    }

    /**
     * Set rooms
     *
     * @param integer $rooms
     *
     * @return Post
     */
    public function setRooms($rooms)
    {
        $this->rooms = $rooms;

        return $this;
    }

    /**
     * Get rooms
     *
     * @return int
     */
    public function getRooms()
    {
        return $this->rooms;
    }

    /**
     * Set floor
     *
     * @param integer $floor
     *
     * @return Post
     */
    public function setFloor($floor)
    {
        $this->floor = $floor;

        return $this;
    }

    /**
     * Get floor
     *
     * @return int
     */
    public function getFloor()
    {
        return $this->floor;
    }

    /**
     * Set hasParking
     *
     * @param boolean $hasParking
     *
     * @return Post
     */
    public function setHasParking($hasParking)
    {
        $this->hasParking = $hasParking;

        return $this;
    }

    /**
     * Get hasParking
     *
     * @return bool
     */
    public function getHasParking()
    {
        return $this->hasParking;
    }

    /**
     * Set hasLift
     *
     * @param boolean $hasLift
     *
     * @return Post
     */
    public function setHasLift($hasLift)
    {
        $this->hasLift = $hasLift;

        return $this;
    }

    /**
     * Get hasLift
     *
     * @return bool
     */
    public function getHasLift()
    {
        return $this->hasLift;
    }

    /**
     * Set hasBunker
     *
     * @param boolean $hasBunker
     *
     * @return Post
     */
    public function setHasBunker($hasBunker)
    {
        $this->hasBunker = $hasBunker;

        return $this;
    }

    /**
     * Get hasBunker
     *
     * @return bool
     */
    public function getHasBunker()
    {
        return $this->hasBunker;
    }

    /**
     * Set hasBalcon
     *
     * @param boolean $hasBalcon
     *
     * @return Post
     */
    public function setHasBalcon($hasBalcon)
    {
        $this->hasBalcon = $hasBalcon;

        return $this;
    }

    /**
     * Get hasBalcon
     *
     * @return bool
     */
    public function getHasBalcon()
    {
        return $this->hasBalcon;
    }

    /**
     * Set isFurnished
     *
     * @param boolean $isFurnished
     *
     * @return Post
     */
    public function setIsFurnished($isFurnished)
    {
        $this->isFurnished = $isFurnished;

        return $this;
    }

    /**
     * Get isFurnished
     *
     * @return bool
     */
    public function getIsFurnished()
    {
        return $this->isFurnished;
    }

    /**
     * Set price
     *
     * @param integer $price
     *
     * @return Post
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return int
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set charges
     *
     * @param integer $charges
     *
     * @return Post
     */
    public function setCharges($charges)
    {
        $this->charges = $charges;

        return $this;
    }

    /**
     * Get charges
     *
     * @return int
     */
    public function getCharges()
    {
        return $this->charges;
    }

    /**
     * Set parkingFee
     *
     * @param string $parkingFee
     *
     * @return Post
     */
    public function setParkingFee($parkingFee)
    {
        $this->parkingFee = $parkingFee;

        return $this;
    }

    /**
     * Get parkingFee
     *
     * @return string
     */
    public function getParkingFee()
    {
        return $this->parkingFee;
    }

    /**
     * Set hasInternet
     *
     * @param string $hasInternet
     *
     * @return Post
     */
    public function setHasInternet($hasInternet)
    {
        $this->hasInternet = $hasInternet;

        return $this;
    }

    /**
     * Get hasInternet
     *
     * @return string
     */
    public function getHasInternet()
    {
        return $this->hasInternet;
    }

    /**
     * Set hasTelephone
     *
     * @param boolean $hasTelephone
     *
     * @return Post
     */
    public function setHasTelephone($hasTelephone)
    {
        $this->hasTelephone = $hasTelephone;

        return $this;
    }

    /**
     * Get hasTelephone
     *
     * @return bool
     */
    public function getHasTelephone()
    {
        return $this->hasTelephone;
    }

    /**
     * Set tel
     *
     * @param integer $tel
     *
     * @return Post
     */
    public function setTel($tel)
    {
        $this->tel = $tel;

        return $this;
    }

    /**
     * Get tel
     *
     * @return int
     */
    public function getTel()
    {
        return $this->tel;
    }

    /**
     * Set info
     *
     * @param string $info
     *
     * @return Post
     */
    public function setInfo($info)
    {
        $this->info = $info;

        return $this;
    }

    /**
     * Get info
     *
     * @return string
     */
    public function getInfo()
    {
        return $this->info;
    }

    /**
     * Set currentTenant
     *
     * @param string $currentTenant
     *
     * @return Post
     */
    public function setCurrentTenant($currentTenant)
    {
        $this->currentTenant = $currentTenant;

        return $this;
    }

    /**
     * Get currentTenant
     *
     * @return string
     */
    public function getCurrentTenant()
    {
        return $this->currentTenant;
    }

    /**
     * Set contactPerson
     *
     * @param string $contactPerson
     *
     * @return Post
     */
    public function setContactPerson($contactPerson)
    {
        $this->contactPerson = $contactPerson;

        return $this;
    }

    /**
     * Get contactPerson
     *
     * @return string
     */
    public function getContactPerson()
    {
        return $this->contactPerson;
    }

    /**
     * Set isPostActive
     *
     * @param boolean $isPostActive
     *
     * @return Post
     */
    public function setIsPostActive($isPostActive)
    {
        $this->isPostActive = $isPostActive;

        return $this;
    }

    /**
     * Get isPostActive
     *
     * @return bool
     */
    public function getIsPostActive()
    {
        return $this->isPostActive;
    }

    /**
     * Set visitationDate
     *
     * @param boolean $visitationDate
     *
     * @return Post
     */
    public function setVisitationDate($visitationDate)
    {
        $this->visitationDate = $visitationDate;

        return $this;
    }

    /**
     * Get visitationDate
     *
     * @return bool
     */
    public function getVisitationDate()
    {
        return $this->visitationDate;
    }

    /**
     * Set availableFrom
     *
     * @param \DateTime $availableFrom
     *
     * @return Post
     */
    public function setAvailableFrom($availableFrom)
    {
        $this->availableFrom = $availableFrom;

        return $this;
    }

    /**
     * Get availableFrom
     *
     * @return \DateTime
     */
    public function getAvailableFrom()
    {
        return $this->availableFrom;
    }

    /**
     * Set availableUpto
     *
     * @param boolean $availableUpto
     *
     * @return Post
     */
    public function setAvailableUpto($availableUpto)
    {
        $this->availableUpto = $availableUpto;

        return $this;
    }

    /**
     * Get availableUpto
     *
     * @return bool
     */
    public function getAvailableUpto()
    {
        return $this->availableUpto;
    }

    /**
     * Set randomId
     *
     * @param integer $randomId
     *
     * @return Post
     */
    public function setRandomId($randomId)
    {
        $this->randomId = $randomId;

        return $this;
    }

    /**
     * Get randomId
     *
     * @return int
     */
    public function getRandomId()
    {
        return $this->randomId;
    }

    /**
     * Set postedBy
     *
     * @param string $postedBy
     *
     * @return Post
     */
    public function setPostedBy($postedBy)
    {
        $this->postedBy = $postedBy;

        return $this;
    }

    /**
     * Get postedBy
     *
     * @return string
     */
    public function getPostedBy()
    {
        return $this->postedBy;
    }

    /**
     * Set crawlerSource
     *
     * @param string $crawlerSource
     *
     * @return Post
     */
    public function setCrawlerSource($crawlerSource)
    {
        $this->crawlerSource = $crawlerSource;

        return $this;
    }

    /**
     * Get crawlerSource
     *
     * @return string
     */
    public function getCrawlerSource()
    {
        return $this->crawlerSource;
    }

    /**
     * Set externalLink
     *
     * @param string $externalLink
     *
     * @return Post
     */
    public function setExternalLink($externalLink)
    {
        $this->externalLink = $externalLink;

        return $this;
    }

    /**
     * Get externalLink
     *
     * @return string
     */
    public function getExternalLink()
    {
        return $this->externalLink;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     *
     * @return Post
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return int
     */
    public function getUserId()
    {
        return $this->userId;
    }
}
