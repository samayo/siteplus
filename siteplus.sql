/*
SQLyog Ultimate v12.09 (64 bit)
MySQL - 5.5.52-MariaDB : Database - siteplus
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`siteplus` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `siteplus`;

/*Table structure for table `fos_user` */

DROP TABLE IF EXISTS `fos_user`;

CREATE TABLE `fos_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `username_canonical` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `email_canonical` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `salt` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `confirmation_token` varchar(180) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `tel` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_957A647992FC23A8` (`username_canonical`),
  UNIQUE KEY `UNIQ_957A6479A0D96FBF` (`email_canonical`),
  UNIQUE KEY `UNIQ_957A6479C05FB297` (`confirmation_token`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `fos_user` */

insert  into `fos_user`(`id`,`username`,`username_canonical`,`email`,`email_canonical`,`enabled`,`salt`,`password`,`last_login`,`confirmation_token`,`password_requested_at`,`roles`,`tel`) values (1,'samayo','samayo','samayo@protonmail.com','samayo@protonmail.com',1,NULL,'$2y$13$cKTQx.gAkxYufCacBpbtledUEHh0tRbKCWmrxl09ataV51FZEFc.q','2017-02-15 13:48:00',NULL,NULL,'a:0:{}',0),(2,'samayor','samayor','samayo@protonmail.ch','samayo@protonmail.ch',1,NULL,'$2y$13$TNX6UexHcxz8VNKCfW3tR.6iB33DmMhBPIx7k0EJlTJpu473.vR6q','2017-02-15 15:55:43',NULL,NULL,'a:0:{}',77924544);

/*Table structure for table `post` */

DROP TABLE IF EXISTS `post`;

CREATE TABLE `post` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `houseNumber` int(11) NOT NULL,
  `zipCode` int(11) NOT NULL,
  `propertyType` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `area` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `rooms` int(11) NOT NULL,
  `floor` int(11) NOT NULL,
  `hasParking` tinyint(1) NOT NULL,
  `hasLift` tinyint(1) NOT NULL,
  `hasBunker` tinyint(1) NOT NULL,
  `hasBalcon` tinyint(1) NOT NULL,
  `isFurnished` tinyint(1) NOT NULL,
  `price` int(11) NOT NULL,
  `charges` int(11) NOT NULL,
  `parkingFee` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `hasInternet` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `hasTelephone` tinyint(1) NOT NULL,
  `tel` int(11) NOT NULL,
  `info` longtext COLLATE utf8_unicode_ci NOT NULL,
  `currentTenant` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contactPerson` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `isPostActive` tinyint(1) NOT NULL,
  `visitationDate` tinyint(1) NOT NULL,
  `availableFrom` datetime NOT NULL,
  `availableUpto` tinyint(1) NOT NULL,
  `randomId` int(11) NOT NULL,
  `postedBy` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `crawlerSource` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `externalLink` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `userId` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=157 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `post` */

insert  into `post`(`id`,`slug`,`title`,`address`,`houseNumber`,`zipCode`,`propertyType`,`area`,`rooms`,`floor`,`hasParking`,`hasLift`,`hasBunker`,`hasBalcon`,`isFurnished`,`price`,`charges`,`parkingFee`,`hasInternet`,`hasTelephone`,`tel`,`info`,`currentTenant`,`contactPerson`,`isPostActive`,`visitationDate`,`availableFrom`,`availableUpto`,`randomId`,`postedBy`,`crawlerSource`,`externalLink`,`userId`) values (122,'lausanne-studio','lausanne studioes','86516 Johanna LocksPort Kileyhaven, AK 99190-8345',0,0,'','',0,0,0,0,0,0,0,1863,0,'','',0,1790,'','','',0,0,'0000-00-00 00:00:00',0,0,'','','',0),(123,'lausanne-hotel','lausanne hotelx','3031 Weimann TrackEast Ravenshire, MA 68993-5204x',0,0,'','',0,1,0,0,0,0,0,81842,0,'','',0,1,'','','',0,0,'0000-00-00 00:00:00',0,0,'','','',0),(124,'vevey-apartment','vevey apartment','75481 Ruth Radial\nCoychester, WY 21138-2948',0,0,'','',0,0,0,0,0,0,0,988,0,'','',0,1,'','','',0,0,'0000-00-00 00:00:00',0,0,'','','',0),(125,'aigle-apartment','aigle apartment','360 Gerlach Meadow\nNew Eudorahaven, ND 64835-1168',0,0,'','',0,0,0,0,0,0,0,1828,0,'','',0,1,'','','',0,0,'0000-00-00 00:00:00',0,0,'','','',0),(126,'lausanne-apartment','lausanne apartment','189 Eddie Throughway Apt. 615\nHilpertport, WY 22345',0,0,'','',0,0,0,0,0,0,0,936,0,'','',0,1,'','','',0,0,'0000-00-00 00:00:00',0,0,'','','',0),(127,'vevey-studio','vevey studio','644 Lorine Pass\nAdrielstad, ND 99711-2166',0,0,'','',0,0,0,0,0,0,0,1500,0,'','',0,1,'','','',0,0,'0000-00-00 00:00:00',0,0,'','','',0),(128,'aigle-colocation','aigle colocation','4869 Schowalter Lodge\nBeerfort, ME 96908-3830',0,0,'','',0,0,0,0,0,0,0,1291,0,'','',0,0,'','','',0,0,'0000-00-00 00:00:00',0,0,'','','',0),(129,'vevey-studio','vevey studio','9871 Bergstrom Gardens\nEast Janelleland, IN 17042-5641',0,0,'','',0,0,0,0,0,0,0,1017,0,'','',0,959,'','','',0,0,'0000-00-00 00:00:00',0,0,'','','',0),(130,'montreux-colocation','montreux colocation','412 Astrid Avenue Apt. 675\nGladyceborough, NC 82845',0,0,'','',0,0,0,0,0,0,0,1037,0,'','',0,401675,'','','',0,0,'0000-00-00 00:00:00',0,0,'','','',0),(131,'crissier-colocation','crissier colocation','1119 Mayert Crescent Suite 798\nNorth Retha, LA 18042',0,0,'','',0,0,0,0,0,0,0,1288,0,'','',0,1,'','','',0,0,'0000-00-00 00:00:00',0,0,'','','',0),(132,'pully-colocation','pully colocation','1786 Kassulke River\nMinnieburgh, AK 91205-4514',0,0,'','',0,0,0,0,0,0,0,1661,0,'','',0,0,'','','',0,0,'0000-00-00 00:00:00',0,0,'','','',0),(133,'vevey-colocation','vevey colocation','50415 Otho Crest\nWestville, CO 71140-3683',0,0,'','',0,0,0,0,0,0,0,1516,0,'','',0,234281,'','','',0,0,'0000-00-00 00:00:00',0,0,'','','',0),(134,'crissier-studio','crissier studio','921 Cullen Courts\nLake Tabitha, UT 01739-2988',0,0,'','',0,0,0,0,0,0,0,1954,0,'','',0,0,'','','',0,0,'0000-00-00 00:00:00',0,0,'','','',0),(135,'montreux-apartment','montreux apartment','701 Buckridge Summit Apt. 203\nGloverberg, PA 85148-0231',0,0,'','',0,0,0,0,0,0,0,950,0,'','',0,0,'','','',0,0,'0000-00-00 00:00:00',0,0,'','','',0),(136,'montreux-apartment','montreux apartment','300 Simonis Crescent\nRudolphchester, NJ 26496',0,0,'','',0,0,0,0,0,0,0,1403,0,'','',0,876863,'','','',0,0,'0000-00-00 00:00:00',0,0,'','','',0),(137,'aigle-hotel','aigle hotel','37163 Bonita Skyway Apt. 321\nTorpbury, VA 34460',0,0,'','',0,0,0,0,0,0,0,1681,0,'','',0,541586,'','','',0,0,'0000-00-00 00:00:00',0,0,'','','',0),(138,'montreux-studio','montreux studio','7583 Boehm Shoals Apt. 985\nWintheiserview, KY 80193',0,0,'','',0,0,0,0,0,0,0,977,0,'','',0,540214,'','','',0,0,'0000-00-00 00:00:00',0,0,'','','',0),(139,'vevey-colocation','vevey colocation','15816 Rice Turnpike Suite 466\nSouth Malcolmtown, SD 82407',0,0,'','',0,0,0,0,0,0,0,1188,0,'','',0,1,'','','',0,0,'0000-00-00 00:00:00',0,0,'','','',0),(140,'crissier-colocation','crissier colocation','503 Brooklyn Meadow\nNorth Eliseo, NH 41259-3530',0,0,'','',0,0,0,0,0,0,0,539,0,'','',0,374597,'','','',0,0,'0000-00-00 00:00:00',0,0,'','','',0),(141,'lausanne-studio','lausanne studio','6623 Lonzo Estates Suite 190\nFlorianville, ND 56578',0,0,'','',0,0,0,0,0,0,0,1349,0,'','',0,315870,'','','',0,0,'0000-00-00 00:00:00',0,0,'','','',0),(142,'pully-hotel','pully hotel','6241 Gilberto Inlet Apt. 128\nEffertzchester, CO 77721',0,0,'','',0,0,0,0,0,0,0,729,0,'','',0,394834,'','','',0,0,'0000-00-00 00:00:00',0,0,'','','',0),(143,'vevey-studio','vevey studio','777 Emmerich Cliffs Apt. 865\nNorth Dahlia, AL 50646-8717',0,0,'','',0,0,0,0,0,0,0,1710,0,'','',0,428,'','','',0,0,'0000-00-00 00:00:00',0,0,'','','',0),(144,'lausanne-hotel','lausanne hotel','3024 Emmanuelle Cliffs\nMaeborough, ID 07307',0,0,'','',0,0,0,0,0,0,0,1900,0,'','',0,1,'','','',0,0,'0000-00-00 00:00:00',0,0,'','','',0),(145,'lausanne-hotel','lausanne hotel','68266 Mohr Mountains Apt. 874\nSouth Woodrowstad, NJ 22542',0,0,'','',0,0,0,0,0,0,0,1081,0,'','',0,470421,'','','',0,0,'0000-00-00 00:00:00',0,0,'','','',0),(146,'aigle-hotel','aigle hotel','937 Romaguera Crest Suite 684\nDickibury, WV 41308',0,0,'','',0,0,0,0,0,0,0,1825,0,'','',0,2147483647,'','','',0,0,'0000-00-00 00:00:00',0,0,'','','',0),(147,'montreux-hotel','montreux hotel','38477 Kunze Shoal\nAufderharhaven, FL 03660',0,0,'','',0,0,0,0,0,0,0,1798,0,'','',0,0,'','','',0,0,'0000-00-00 00:00:00',0,0,'','','',0),(148,'crissier-hotel','crissier hotel','6385 Nichole Mission Suite 427\nMohrfurt, AK 21578-8065',0,0,'','',0,0,0,0,0,0,0,608,0,'','',0,1,'','','',0,0,'0000-00-00 00:00:00',0,0,'','','',0),(149,'lausanne-studio','lausanne studio','5972 Oberbrunner Wells\nLake Rubymouth, NV 94773-3347',0,0,'','',0,0,0,0,0,0,0,1426,0,'','',0,1,'','','',0,0,'0000-00-00 00:00:00',0,0,'','','',0),(150,'vevey-hotel','vevey hotel','2795 Leopoldo Stream Apt. 267\nMurrayland, MI 75278',0,0,'','',0,0,0,0,0,0,0,706,0,'','',0,2147483647,'','','',0,0,'0000-00-00 00:00:00',0,0,'','','',0),(151,'pully-hotel','pully hotel','7041 Devonte Parkways Suite 498\nHirthebury, TX 75124-9897',0,0,'','',0,0,0,0,0,0,0,1048,0,'','',0,285,'','','',0,0,'0000-00-00 00:00:00',0,0,'','','',0),(152,'a-root-in-bex','a room in bex','Rue central',0,0,'','28',0,2,0,0,0,0,0,998,0,'','',0,0,'','','',0,0,'0000-00-00 00:00:00',0,0,'','','',0),(153,'room-in-sion','room in sion','sion',0,0,'','',0,3,0,0,0,0,0,823,0,'','',0,0,'','','',0,0,'0000-00-00 00:00:00',0,0,'','','',0),(154,'room-in-sion','room in martigny','martigny',0,0,'','',0,4,0,0,0,0,0,323,0,'','',0,0,'','','',0,0,'0000-00-00 00:00:00',0,0,'','','',0),(155,'room-in-sion','room in genev','genve',0,0,'','',0,1,0,0,0,0,0,123,0,'','',0,0,'','','',0,0,'0000-00-00 00:00:00',0,0,'','','',0),(156,'room-in-sion','cash in lausanne','lausanne',0,0,'','',0,3,0,0,0,0,0,3232,0,'','',0,0,'','','',0,0,'0000-00-00 00:00:00',0,0,'','','',0);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
